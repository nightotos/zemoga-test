import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ThumbComponent } from './pages/thumb/thumb.component';
import { AppRoutingModule } from './app-routing.module';
import { AppMainComponent } from './layout/app.main.component';
import { TopBarComponent } from './layout/top-bar/top-bar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { ImgThumbPrincipalComponent } from './pages/thumb/components/img-thumb-principal/img-thumb-principal.component';
import { CardThumbPrincipalComponent } from './pages/thumb/components/card-thumb-principal/card-thumb-principal.component';
import { ClosingGaugeComponent } from './pages/thumb/components/closing-gauge/closing-gauge.component';
import { BannerThumbComponent } from './pages/thumb/components/banner-thumb/banner-thumb.component';
import { ListThumbsComponent } from './pages/thumb/components/list-thumbs/list-thumbs.component';
import { BannerThumbBotComponent } from './pages/thumb/components/banner-thumb-bot/banner-thumb-bot.component';
import { CardThumpComponent } from './pages/thumb/components/card-thump/card-thump.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ThumbComponent,
    AppMainComponent,
    TopBarComponent,
    FooterComponent,
    ImgThumbPrincipalComponent,
    CardThumbPrincipalComponent,
    ClosingGaugeComponent,
    BannerThumbComponent,
    ListThumbsComponent,
    BannerThumbBotComponent,
    CardThumpComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
