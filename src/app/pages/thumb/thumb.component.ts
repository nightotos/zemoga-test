import { Component, HostListener, OnInit } from '@angular/core';
import { Thumb } from 'src/app/core/interfaces/thumb/thumb.interface';
import { ThumbService } from 'src/app/core/services/thumb/thumb.service';

@Component({
  selector: 'app-thumb',
  templateUrl: './thumb.component.html',
  styleUrls: ['./thumb.component.css'],
})
export class ThumbComponent implements OnInit {
  dataPeople: Thumb[] = [];
  tipeView: 'grid' | 'list' = 'grid';
  isActiveSelectTypeView: boolean = true;
  constructor(private thumbService: ThumbService) {}

  ngOnInit(): void {
    this.obtainListPeople();
    this.resizeWindow();
  }

  obtainListPeople(): void {
    this.thumbService.getListPeople().subscribe((data) => {
      this.dataPeople = data;
    });
  }

  @HostListener('window:resize', ['$event'])
  resizeWindow() {
    if (window.innerWidth < 768) {
      this.tipeView = 'grid';
      this.isActiveSelectTypeView = false;
    } else {
      this.isActiveSelectTypeView = true;
    }
  }
}
