import { Component, HostListener, Input, OnInit } from '@angular/core';
import { Thumb } from 'src/app/core/interfaces/thumb/thumb.interface';
import * as dayjs from 'dayjs';
import { ThumbService } from 'src/app/core/services/thumb/thumb.service';
@Component({
  selector: 'app-card-thump',
  templateUrl: './card-thump.component.html',
  styleUrls: ['./card-thump.component.css'],
})
export class CardThumpComponent implements OnInit {
  @Input() person: Thumb;
  @Input() set tipeViewChange(data: 'grid' | 'list') {
    this.tipeView = data;
    this.calViewPort();
  }

  lastUpdate: string = '';
  valoration: 'up' | 'down';
  ariaLabelValoration: 'thumbs up' | 'thumbs down';
  percentageUp: number = 0;
  percentageDown: number = 0;
  personNameLength: number = 12;
  personNameLengthTrunc: number = 20;
  personDescriptionLengthTrunc: number = 80;
  tipeView: 'grid' | 'list' = null;
  disabledVote: boolean = true;
  voteActive: 'none' | 'up' | 'down' = 'none';
  voteMaded: boolean = false;
  constructor(private thumbService: ThumbService) {}

  ngOnInit(): void {
    this.calculateLastUpdate();
    this.calculatePercentageVotes();
    this.calViewPort();
  }

  calculateLastUpdate(): void {
    const dateLastUpdated = dayjs(new Date());
    let diff = dateLastUpdated.diff(this.person.lastUpdated, 'month');

    if (diff > 12) {
      let diffYear = this.diffInDate(dateLastUpdated, 'year');
      this.lastUpdate = `${diffYear} year${diffYear > 1 ? 's' : ''} ago in ${
        this.person.category
      } `;
    }

    if (diff >= 1 && diff <= 12) {
      let diffMonth = this.diffInDate(dateLastUpdated, 'month');
      this.lastUpdate = `${diffMonth} month${diffMonth > 1 ? 's' : ''} ago in ${
        this.person.category
      } `;
    }

    if (diff < 1) {
      let diffDay = this.diffInDate(dateLastUpdated, 'day');
      this.lastUpdate = `${diffDay} day${diffDay > 1 ? 's' : ''} ago in ${
        this.person.category
      } `;
    }
  }

  diffInDate(
    dateLastUpdated: dayjs.Dayjs,
    type: 'month' | 'day' | 'year'
  ): number {
    return Number(dateLastUpdated.diff(this.person.lastUpdated, type));
  }

  calculatePercentageVotes(): void {
    let sumaVotes = this.person.votes.negative + this.person.votes.positive;

    this.percentageUp = Number(
      ((100 / sumaVotes) * this.person.votes.positive).toFixed(2)
    );

    this.percentageDown = Number((100 - this.percentageUp).toFixed(2));

    if (this.percentageUp >= this.percentageDown) {
      this.valoration = 'up';
      this.ariaLabelValoration = 'thumbs up';
    } else {
      this.valoration = 'down';
      this.ariaLabelValoration = 'thumbs down';
    }
  }

  calViewPort(): void {
    if (window.innerWidth < 768) {
      this.personNameLength = 12;
      this.personNameLengthTrunc = 20;
      this.personDescriptionLengthTrunc = 80;
    }

    if (window.innerWidth > 767 && window.innerWidth < 858) {
      this.personNameLength = 12;
      this.personNameLengthTrunc = 20;
      this.personDescriptionLengthTrunc = 120;
    }

    if (window.innerWidth > 857 && window.innerWidth < 858) {
      this.personNameLength = 18;
      this.personNameLengthTrunc = 30;
      this.personDescriptionLengthTrunc = 120;
    }

    if (window.innerWidth > 1099 && this.tipeView === 'grid') {
      this.personNameLength = 16;
      this.personNameLengthTrunc = 20;
      this.personDescriptionLengthTrunc = 80;
    }

    if (window.innerWidth > 1099 && this.tipeView === 'list') {
      this.personNameLength = 20;
      this.personNameLengthTrunc = 30;
      this.personDescriptionLengthTrunc = 120;
    }
  }

  @HostListener('window:resize', ['$event'])
  resizeWindow() {
    this.calViewPort();
  }

  classTopName(): string {
    if (this.tipeView === 'list') {
      return '';
    }

    return this.person.name.length > this.personNameLength
      ? 'top-10'
      : 'top-20';
  }

  selectButton(vote: 'up' | 'down') {
    this.voteActive = vote;
  }

  vote() {
    if (!this.person.voted) {
      if (this.voteActive === 'up') {
        this.person.votes.positive = this.person.votes.positive + 1;
      } else {
        this.person.votes.negative = this.person.votes.negative + 1;
      }
      this.person.voted = true;
      this.saveVote();
    }
  }

  resetVote() {
    this.person.voted = false;
    this.voteActive = 'none';
  }

  saveVote() {
    this.thumbService.updatePeople(this.person.id, this.person).subscribe(
      () => {
        this.calculatePercentageVotes();
      },
      () => {
        if (this.voteActive === 'up') {
          this.person.votes.positive = this.person.votes.positive - 1;
        } else {
          this.person.votes.negative = this.person.votes.negative - 1;
        }
        this.resetVote();
      }
    );
  }
}
