export interface Thumb {
  id: number;
  name: string;
  description: string;
  category: string;
  picture: string;
  lastUpdated: Date;
  votes: Votes;
  voted: boolean;
}

export interface Votes {
  positive: number;
  negative: number;
}
