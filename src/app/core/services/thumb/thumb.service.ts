import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Thumb } from '../../interfaces/thumb/thumb.interface';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class ThumbService {
  constructor(private http: HttpClient) {}

  getListPeople(): Observable<Thumb[]> {
    return this.http.get<Thumb[]>(`${environment.api}data`);
  }

  updatePeople(id: number, data: Thumb): Observable<Thumb> {
    return this.http.put<Thumb>(`${environment.api}data/${id}`, data);
  }
}
