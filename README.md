# Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.4.

Pre-requisites

- Node
- Typescript

# Installing App

In order to use the project, we must have Angular CLI installed.
https://cli.angular.io/

if you have installed Angular skip this step.

1.  Install Angular CLI (Angular command line interface)
    Run `npm install -g @angular/cli`

2.  Run `npm install`

# Installing Server

In order to use the project, we must have JSON Server installed globally in the machine
https://github.com/typicode/json-server

-Run `npm install -g json-server`

## Development server

1. Run `json-server --watch data.json` for up the server data application

2. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
